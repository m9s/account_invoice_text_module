.. _doc_de_account_invoice_text_module

***************************
Textbausteine in Rechnungen
***************************

:Modul: account_invoice_text_module
:Menü: :menuselection:`Administration --> Textbausteine`
:Basismodul: :text_module
:Modell: text_module.text_module
:Version: 2.2

In Rechnungen können Textbausteine an folgenden Positionen verwendet werden:
 * vor den Rechnungspositionen (Kopfzeilen)
 * in den Rechnungspositionen (nur in Zeilen vom Typ *Kommentar*)
 * nach den Rechnungspositionen (Fußzeilen)

Folgende Typen von Textbausteinen können definiert werden:


Allgemeine Textbausteine
^^^^^^^^^^^^^^^^^^^^^^^^

Textbausteine vom Typ *Allgemein* stellen die Standardversion eines
Textbausteins dar. Sie können überall verwendet werden.


Parteientextbausteine
^^^^^^^^^^^^^^^^^^^^^

Für Textbausteine vom Typ *Partei* können Parteien definiert werden. Diese
Textbausteine können in Berichten abhängig von einer Partei hinzugefügt werden.

In der Rechnung werden definierte Parteientextbausteine bei der Auswahl einer
Partei automatisch in die Fußzeilen eingefügt. Wenn die Partei einer Rechnung
während der Bearbeitung geändert wird, werden diese Zeilen automatisch an die
für die neue Partei hinterlegten Textbausteine angepasst.


Ländertextbausteine
^^^^^^^^^^^^^^^^^^^

Für Textbausteine vom Typ *Land* können Länder definiert werden. Diese
Textbausteine können in Berichten abhängig von einer Adresse oder einem
Land hinzugefügt werden.

In der Rechnung werden definierte Ländertextbausteine bei der Auswahl einer
Partei abhängig vom Land der Rechnungsadresse automatisch in die Fußzeilen
eingefügt. Wenn die Rechnungsadresse geändert wird, werden diese Zeilen
automatisch an das Land der neuen Rechnungsadresse angepasst.

Durch das Hinterlegen von Parteien kann die Verwendung eines Ländertextbausteins
auf diese Parteien beschränkt werden.


Beschränkung von Textbausteinen auf einzelne Berichte
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Damit Textbausteine in Berichten verwendet werden können, muss der jeweilige
Bericht im Feld *Berichte* ausgewählt sein.
