# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval, In

class TextModule(ModelSQL, ModelView):
    _name = 'text_module.text_module'

    parties = fields.Many2Many('party.party-text_module.text_module',
        'text_module', 'party', 'Parties', states={
            'invisible': Not(In(Eval('type'), ['party', 'country'])),
        })
    countries = fields.Many2Many('country.country-text_module.text_module',
        'text_module', 'country', 'Contries', states={
            'invisible': Not(Equal(Eval('type'), 'country')),
        })
    reports = fields.Many2Many('ir.action.report-text_module.text_module',
        'text_module', 'report', 'Reports')
    type = fields.Selection([
            ('general', 'General'),
            ('party', 'Party'),
            ('country', 'Country'),
        ], 'Type', required=True, on_change=['parties', 'countries', 'type'])

    def default_type(self):
        return 'general'

    def on_change_type(self, vals):
        res = {}
        if not vals.get('type'):
            return res
        if vals['type'] != 'country' and vals.get('countries'):
            res['countries'] = {'remove': [
                x['id'] for x in vals['countries']
                ]}
        if vals['type'] != 'party' and vals.get('parties'):
            res['parties'] = {'remove': [
                x['id'] for x in vals['parties']
                ]}
        return res

TextModule()


class PartyTextModule(ModelSQL):
    'Party - Text Module'
    _name = 'party.party-text_module.text_module'
    _description = __doc__

    party = fields.Many2One('party.party', 'Party', required=True)
    text_module = fields.Many2One('text_module.text_module', 'Text Module',
        required=True)

PartyTextModule()


class CountryTextModule(ModelSQL):
    'Country - Text Module'
    _name = 'country.country-text_module.text_module'
    _description = __doc__

    country = fields.Many2One('country.country', 'Country', required=True)
    text_module = fields.Many2One('text_module.text_module', 'Text Module',
        required=True)

CountryTextModule()


class IrActionReportTextModule(ModelSQL):
    'Ir Action Report - Text Module'
    _name = 'ir.action.report-text_module.text_module'
    _description = __doc__

    report = fields.Many2One('ir.action.report', 'Report', required=True)
    text_module = fields.Many2One('text_module.text_module', 'Text Module',
        required=True)

IrActionReportTextModule()

