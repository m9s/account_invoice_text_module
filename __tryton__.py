#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Text Module',
    'name_de_DE': 'Fakturierung Textbausteine',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Text Module
    - Provides standard texts on invoices
    - Adds a notebook for headers and footers from standard texts
    - Adds the possibility to select from standard texts in comments from
      invoice lines
    - Allows to define party and country texts that will be added to an
      invoice automatically when a party or invoice address is selected.
''',
    'description_de_DE': '''Textbausteine für Rechnungen
    - Ermöglicht die Verwendung von Standardtexten in Rechnungen
    - Fügt einen zusätzlichen Tab für Kopf- und Fußzeilen aus Textbausteinen
      hinzu
    - Ermöglicht die Auswahl von Textbausteinen in Kommentaren von
      Rechnungszeilen
    - Ermöglicht die Definition von Parteien- und Ländertextbausteinen,
      die bei Auswahl einer Partei bzw. Rechnungsadresse automatisch
      hinzugefügt werden.
''',
    'depends': [
        'account_invoice',
        'text_module',
    ],
    'xml': [
        'invoice.xml',
        'text_module.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
